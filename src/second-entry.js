import ReactDOM from 'react-dom';
import React from 'react';
import max from './max';

console.log('Hello from second entry');

function attachEvent () {
	
	let button = document.getElementById('lazy-load-button');
	button.addEventListener('click', function () {
		import(/* webpackChunkName: "lazy" */'./lazy-chunk').then(module => {
			module.default();
		});
	});
}

if (document.readyState === 'complete') {
	attachEvent();
} else {
	window.onload = attachEvent;
}