// Start off by coercing some environment variables
process.env.BABEL_ENV = 'development';
process.env.NODE_ENV = 'development';

const getEnvironmentSettings = require('../config/env');
const config = require('../config/webpack.config.dev');
const WebpackDevServer = require('webpack-dev-server');
const webpack = require('webpack');
const path = require('path');

// Create our Webpack Compiler
let compiler = webpack(config);

/**
* Create our Webpack Dev Server
* If you have everything in a webpack.config.js, you can add the
* webpack dev server configurations in that. If you are using webpack
* via the node api, then you need to pass in the dev server config directly here.
*/
let server = new WebpackDevServer(compiler, {
	contentBase: path.posix.join(process.cwd(), 'public'),
	watchContentBase: true,
	compress: true,
	hot: true,
	stats: {
		performance: true,
		modules: false,
		colors: true,
		assets: true
	}
});

// Grab environment settings, we will need the port from this
let { raw: env } = getEnvironmentSettings();

// Start our development server
server.listen(env.PORT, '127.0.0.1', () => console.log(`\x1B[1mStarting server on \x1B[36mhttp://localhost:${env.PORT}\x1B[39m\x1B[22m`));