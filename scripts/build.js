// Start off by coercing some environment variables
process.env.BABEL_ENV = 'production';
process.env.NODE_ENV = 'production';

const config = require('../config/webpack.config.prod');
const webpack = require('webpack');

// Create our compiler and run it
let compiler = webpack(config);

compiler.run((err, stats) => {
	if (err) { throw err; }

	console.log(stats.toString({
		errorDetails: true,
		warnings: true,
		modules: false,
		chunks: false,
		colors: true
	}));
});