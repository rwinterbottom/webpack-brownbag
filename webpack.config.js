const path = require('path');

module.exports = {
	mode: 'development',
	entry: {
		app: './src/simple-entry.js'
	},
	output: {
		path: path.posix.join(process.cwd(), 'build'),
		filename: '[name].[hash].js'
	}
};