Webpack Brown-Bag
=================

## What's Here
This is the content of the Webpack brown bag. There are three separate examples of webpack config. Each one can be triggered by npm scripts. They are configured as follows:

### simple-build
`yarn simple-build`

This is a very minimal build with one entry. It uses the `webpack.config.js` in the top level and generates a build with mode set to production. It only has entry, output, and mode set. It is designed to show the minimal amount of configuration necessary to get webpack started. It requires `webpack` and `webpack-cli` to be installed.

### start
`yarn start`

This is a development version that has multiple entries, common chunks, code splitting, and webpack dev server. It uses the node api. See `scripts/start` and `config/webpack.config.dev.js`.

### build
`yarn build`

This is an optimized production build. It has all the same things as development except for the webpack dev server. It adds an extract text plugin and a custom inline style plugin (see `plugins/inline-style-plugin.js`). This demonstrates how to author your own plugin, have it hook into other plugins, and further optimize your app for production.

See `scripts/build` and `config/webpack.config.prod.js`.

## Brown Bag Outline
> NOTE: Before running any of the following examples, you need to run `yarn install` or `npm install` to install all the necessary dependencies.

### Simple Configuration
Webpack 4 advertises zero-config. In reality, that only works if your app is built under some assumptions(They essentially set a default entry, output, and mode for you). More often than not, you will need to add a `webpack.config.js` and setup these three fields yourself. See `webpack.config.js` for an example. Also, webpack is very flexible, so check the documentation for available formats. For example, [https://webpack.js.org/concepts/entry-points/](https://webpack.js.org/concepts/entry-points/). When using the simple configuration, you will need `webpack` and `webpack-cli` installed as well.
	
### Webpack Node API
[https://webpack.js.org/api/node/](https://webpack.js.org/api/node/)

Webpack also has a node api, which means you do not need to install the `webpack-cli`. A simple example looks like this:

```javascript
const config = require('./webpack.config');
const webpack = require('webpack');
// Create a compiler
let compiler = webpack(config);
compiler.run((err, stats) => {
	if (err) {
		// We had a compiler error
	}
	
	console.log(stats.toString(/* options */));
});
```

Using the node api you can programmatically generate a config which gives you much more flexibility. Both the `start` and `build` examples use this. You can view the comments about those commands at the top of this file.

### Webpack Loaders
[https://webpack.js.org/concepts/#loaders](https://webpack.js.org/concepts/#loaders)

Loaders are how you teach webpack to load and transform different types of files. You can teach webpack how to load `js` via `babel`, `ts`, `css`, `scss`, `less`, `json`, `html`, `png`, `svg`, and many more. Loaders will require you to install a loader dependency and configure one. You can see examples in both `start` and `build` setups. A simple example of adding babel would look like this:

First install `babel-core` and `babel-loader`. `babel-core` is required for `babel-loader` but most loaders don't have additional dependencies.
```bash
yarn add babel-core babel-loader
``` 
Configure webpack so it knows how to use `babel-loader`.
```javascript
// in your webpack config
{
	entry: { ... },
	output: { ... },
	// Configure the loader here
	module: {
		rules: [{
			test: /\.jsx?$/,
			loader: 'babel-loader'
		}]
	}
}
```

And your all done. Loaders can be configured in many ways, you can also chain loaders. There is an example of this in the `start` configuration but here is what the module.rules config could look like:

```javascript
{
	module: {
		rules: [{
			test: /\.css$/,
			// css-loader evaluates first, then results
			// get passed into style-loader
			loaders: ['style-loader', 'css-loader']
			// Sometimes you can/must use the `use` property
			// instead of the `loaders` property.
		}]
	}
}
```

You should take some time to read about loaders in the docs as these are very powerful and knowing how to configure them will make your life much easier.

### Webpack Plugins
[https://webpack.js.org/concepts/#plugins](https://webpack.js.org/concepts/#plugins)

Webpack plugins are great for configuring and performing a larger set of tasks. These can be used for generating html content, optimizing your app, enabling hot module replacement, and many other custom features. Like loaders, you should take time to read about these and really understand them. There are a ton of plugins available in the community that do some really cool things. See [Writing a custom webpack plugin](#writing-a-custom-webpack-plugin) below for information on how to write your own plugin and here is a quick example of adding the HtmlWebpackPlugin:

```javascript
{
	entry: { ... },
	output: { ... },
	plugins: [
		new HtmlWebpackPlugin({
			template: 'src/index.html',
			inject: true
		})
	]
}
```

### Webpack Dev Server
[https://webpack.js.org/configuration/dev-server/](https://webpack.js.org/configuration/dev-server/)

The Webpack Dev Server can be easily setup and used for hot module replacement. See the `start` configuration for an example setup. One thing to note is using this in the node api, the dev server configuration needs to be passed in to the dev server instead of in the webpack.config.js. I will try to highlight the setup below.

1. Install `webpack-dev-server`.
2. Update your entry points:
```javascript
// This
{
	entry: {
		app: 'src/app.js'
	}
}
// Becomes this
{
	entry: {
		app: [
			`webpack-dev-server/client?http://localhost:${ env.PORT || 3000 }`,
			'webpack/hot/dev-server',
			'src/app.js'
		]
	}
}
```
3. Update your start script:
```javascript
// This
let compiler = webpack(config);
compiler.run(callback);
// Becomes this
let compiler = webpack(config);
let server = new WebpackDevServer(compiler, {
	// This needs to match output directory (path)
	contentBase: path.posix.join(process.cwd(), 'public'),
	hot: true
});
server.listen(env.PORT || 3000, '127.0.0.1', callback);
```

This will autoreload your server on changes. You can go further if your UI library supports it. For example in React, you can do this so the whole page does not reload, just the component.

```javascript
// This will get removed in prod mode because of dead code elimination
if (process.env.NODE_ENV === 'development' && module && module.hot) {
	module.hot.accept('src/AppComponent.js', () => {
		let HotComponent = require('src/AppComponent.js').default;
		ReactDOM.render(
			<HotComponent />,
			document.getElementById('mount')
		)
	});
}
```
	
### Webpack Optimization
[https://medium.com/webpack/webpack-4-mode-and-optimization-5423a6bc597a](https://medium.com/webpack/webpack-4-mode-and-optimization-5423a6bc597a)

Documentation for this may not be available yet, it wasn't at the time of writing this. You can read the linked blog post above for some help but you will most likely be googling anything you want to do related to this for a while.

Commons chunk plugin has gone away, now you can give webpack some basic config to have it figure out how to chunk your code for you. Note this typically is only used when you have more than one entry, if you only have one entry, you may will need to change minChunks or hardcode the dependencies. I use it in both `start` and `build` configurations to generate a vendor bundle like so. 

```javascript
{
	entry: { ... },
	output: { ... },
	optimization: {
		splitChunks: {
			cacheGroups: {
				vendor: {
					name: 'vendor',
					chunks: 'all',
					minChunks: 2
				}
			}
		}
	}
}
```
	
### Webpack Code Splitting
[https://webpack.js.org/guides/code-splitting/](https://webpack.js.org/guides/code-splitting/)

Code splitting is very easy to do in Webpack and is supported almost out of the box. You may need babel and the `babel-plugin-syntax-dynamic-import` babel plugin to use it. Note that Babel configuration takes place in the `.babelrc` not in the webpack config. You can see the client side code of this in `src/second-entry.js`. To enable this in webpack, and this is optional, add this configuration to output so you can control the name:

```javascript
{
	output: {
		chunkFilename: '[name].chunk.js'
	}
}
```

In your client code, you will write something like this:

```javascript
import('./lazy-module').then(module => {
	// Grab your exports on module or if your module
	// has a default export, access it via module.default
	module.default();
});
```

Webpack will see this and pull the code out. You can put this in event listeners or routers to lazy load chunks of code and speed up the initial page load time.
	
### Writing a custom webpack plugin
[https://webpack.js.org/api/plugins/](https://webpack.js.org/api/plugins/)

Writing plugins is both simple and powerful. The custom plugin I wrote will take some css that was loaded and extracted from the client, via `import 'core.css'`, take the source, make it available to another plugin, and then allow that plugin to inline the css into the head. This saves the http request for a blocking resource and removes the flash of unstyled content which improves experience and speed. You can see it in `plugins/inline-style-plugin`. The basic interface for a plugin looks like this:

```javascript
class MyPlugin {
	constructor () {
		// This is optional but useful if you want
		// your plugin to accept arguments
	}
	
	// This is required
	apply (compiler) {
		// You can hook into different events via compiler.hooks
		// Here we hook into a compilation event
		compiler.hooks.compilation.tap('MyPlugin', compilation => {
			
			// Hook into other plugins or do some stuff here,
			// if the event has a callback passed in, make sure
			// you call it otherwise your build may not actually build
			// The callback signifies you are done and will 
			// silently fail if not called
		});
	}
}
```

Check the following links for tips on writing a plugin (first one is for v3 and the second one is for v4, with a great video): 
* [https://webpack.js.org/contribute/writing-a-plugin/](https://webpack.js.org/contribute/writing-a-plugin/).
* [https://medium.com/webpack/webpack-4-migration-guide-for-plugins-loaders-20a79b927202](https://medium.com/webpack/webpack-4-migration-guide-for-plugins-loaders-20a79b927202)

