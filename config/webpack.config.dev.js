const HtmlWebpackPlugin = require('html-webpack-plugin');
const getEnvironmentSettings = require('./env');
const webpack = require('webpack');
const path = require('path');

// Grab variables needed for environment settings
let { raw: env, webpack_env } = getEnvironmentSettings();

/**
* Set up our entries for our dev server.
* Typically an entry looks like this:
```
entry: './path/to/entry.js'
```
* - or you can name your entries like this:
```
entry: {
	filename: './path/to/entry.js'
}
```
* Webpack Dev Server needs an array of entry files so it can properly hot reload
* file updates.
*/
function makeEntriesDevServerCompatible (entries = {}) {
	return Object.getOwnPropertyNames(entries)
		.reduce(function (all, entry_name) {
			all[entry_name] = [
				`webpack-dev-server/client?http://localhost:${ env.PORT || 3000 }`,
				'webpack/hot/dev-server',
				entries[entry_name]
			];
			return all;
		}, entries);
}

// Define our actual entry points
let entries = {
	first: path.posix.resolve('src/first-entry.js'),
	second: path.posix.resolve('src/second-entry.js')
};

// Define our webpack configuration
let webpack_config = {
	mode: process.env.NODE_ENV,
	entry: makeEntriesDevServerCompatible(entries),
	output: {
		path: path.posix.join(process.cwd(), 'public'),
		filename: '[name].[hash].js',
		chunkFilename: '[name].chunk.js'
	},
	module: {
		rules: [{
			test: /\.js$/,
			loader: 'babel-loader'
		}, {
			test: /\.css$/,
			loaders: ['style-loader', 'css-loader']
		}]
	},
	plugins: [
		new webpack.DefinePlugin(webpack_env),
		new webpack.HotModuleReplacementPlugin(),
		new HtmlWebpackPlugin({
			template: path.posix.resolve('src/index.html'),
			inject: false
		})
	],
	optimization: {
		splitChunks: {
			cacheGroups: {
				vendor: {
					name: 'vendor',
					chunks: 'all',
					minChunks: 2
				}
			}
		}
	}
};

module.exports = webpack_config;