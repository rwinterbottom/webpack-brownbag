const InlineStylePlugin = require('../plugins/inline-style-plugin');
const MiniCSSExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const getEnvironmentSettings = require('./env');
const webpack = require('webpack');
const path = require('path');

// Grab variables needed for environment settings
let { raw: env, webpack_env } = getEnvironmentSettings();

// Define our actual entry points
let entries = {
	first: path.posix.resolve('src/first-entry.js'),
	second: path.posix.resolve('src/second-entry.js')
};

// Define our webpack configuration
let webpack_config = {
	mode: process.env.NODE_ENV,
	profile: true,
	entry: {
		first: path.posix.resolve('src/first-entry.js'),
		second: path.posix.resolve('src/second-entry.js')
	},
	output: {
		path: path.posix.join(process.cwd(), 'build'),
		filename: '[name].[hash].js',
		chunkFilename: '[name].chunk.js'
	},
	module: {
		rules: [{
			test: /\.js?$/,
			loader: 'babel-loader'
		}, {
			test: /\.css$/,
			use: [MiniCSSExtractPlugin.loader, 'css-loader']
		}]
	},
	plugins: [
		new webpack.DefinePlugin(webpack_env),
		new webpack.optimize.OccurrenceOrderPlugin(),
		new HtmlWebpackPlugin({
			template: path.posix.resolve('src/index.html'),
			inject: false
		}),
		new MiniCSSExtractPlugin({
			filename: 'core.css'
		}),
		new InlineStylePlugin({
			html_name: 'core',
			filename: 'core.css'
		})
	],
	optimization: {
		splitChunks: {
			cacheGroups: {
				vendor: {
					name: 'vendor',
					chunks: 'all',
					minChunks: 2
				}
			}
		}
	}
};

module.exports = webpack_config;